# Backup Zimbra To Amazon S3

## info

This script is written specificaly for backing up a single node zimbra server to an amazon S3 bucket.

To run this script simply execute the bash and provide the 2 needed variables. This script requires a varaible to indicate the amazon bucket and a number to indicate the days you want to keep the backup.
It is also required that you zimbra server has the amazon s3cmd isntalled, otherwise i cant connect to the s3.

If you run following command:

<code>bash mailbackupshell.sh bucketname 7</code>

Then your zimbra will be backup to your "bucketname" and the local and external backups will be checked, any backups older then 7 days will be deleted.
